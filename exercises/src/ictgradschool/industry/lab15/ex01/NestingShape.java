package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ylin183 on 15/05/2017.
 */
public class NestingShape extends Shape {
    List<Shape> nest = new ArrayList<>();

    // Creates a NestingShape object with default values for state.
    public NestingShape() {
        super();
    }

    // Creates a NestingShape object with specified location values, and default values for other state items.
    public NestingShape(int x, int y) {
        super(x, y);
    }

    // Creates a NestingShape with specified values for location, velocity and direction. Non-specified state items take on default values.
    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    // Creates a NestingShape with specified values for location, velocity, direction, width and height.
    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    // Moves a NestingShape object (including its children) with the bounds specified by arguments width and height. Remember that the NestingShape’s children only want to move within the bounds of the NestingShape itself, rather than the whole screen.
    public void move(int width, int height) {
        super.move(width, height);
        for (Iterator<Shape> it = nest.iterator(); it.hasNext(); ) {
            it.next().move(fWidth, fHeight);
        }
    }

    // Paints a NestingShape object by drawing a rectangle around the edge of its bounding box. The NestingShape object's children are then painted.
    public void paint(Painter painter) {

        // Paint NestingShape
        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);
//        System.out.println("paint NestingShape");

        // Paint children
        for (int i = 0; i < shapeCount(); i++) {
            Shape shape = shapeAt(i);
            shape.paint(painter);
//            System.out.println("paint child of NestingShape");
        }
//        painter.translate(0, 0);
        painter.translate(-fX, -fY);
    }

//    @Override
//    public void paint(Painter painter) {
//        painter.drawRect(fX, fY, fWidth, fHeight);
//        painter.translate(fX, fY);
//
//        for (Shape s : nest) {
//            s.paint(painter);
//        }
//        painter.translate(-fX, -fY);
//    }

    // Attempts to add a Shape to a NestingShape object. If successful, a two-way link is established between the NestingShape and the newly added Shape.
    // This method throws an IllegalArgumentException if an attempt is made to add a Shape to a NestingShape instance where the Shape argument is already a child within a NestingShape instance.
    // An IllegalArgumentException is also thrown when an attempt is made to add a Shape that will not fit within the bounds of the proposed NestingShape object.
    public void add(Shape child) throws IllegalArgumentException {
//        try {
        if (child.parent() != null) {
            throw new IllegalArgumentException();
        }
        if (contains(child)) {
            throw new IllegalArgumentException();
        } else if (child.getHeight() + child.getY() > this.fHeight + this.fY ||
                child.getHeight() > this.getHeight() ||
                child.getWidth() + child.getX() > this.fWidth + this.fX ||
                child.getWidth() > this.getWidth()
                ) {
            throw new IllegalArgumentException();
        } else {
            // add shape to a NestingShape object
            this.nest.add(child);
            child.parent = this;
            System.out.println("added child");
        }
//        } catch (IllegalArgumentException e) {
//            e.getMessage();
//        }
    }

    // Removes a particular Shape from a NestingShape instance. Once removed, the two-way link between the NestingShape and its former child is destroyed. This method has no effect if the Shape specified to remove is not a child of the NestingShape.
    public void remove(Shape child) {
        int index = indexOf(child);
        this.nest.remove(index);
        child.parent = null;
        System.out.println("removed child");
    }

    // Returns the Shape at a specified position within a NestingShape.
    // If the position specified is less than zero or greater than the number of children stored in the NestingShape less one this method throws an IndexOutOfBoundsException.
    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
//        try {
        if (index < 0 || index >= shapeCount()) {
//        if (index < 0 || index > nest.size()) { // another if condition
            throw new IndexOutOfBoundsException();
        } else {
            return nest.get(index);
        }
//        } catch (IndexOutOfBoundsException e) {
//            e.getMessage();
//        }
//        return shape;
    }

    // Returns the number of children contained within a NestingShape object. Note this method is not recursive - it simply returns the number of children at the top level within the callee NestingShape object.
    public int shapeCount() {
        return nest.size();
    }

    // Returns the index of a specified child within a NestingShape object. If the Shape specified is not actually a child of the NestingShape this method returns -1; otherwise the value returned is in the range 0 .. shapeCount() - 1.
    public int indexOf(Shape child) {
        if (nest.contains(child)) {
            return nest.indexOf(child);
        }
        return -1;
    }

    // Returns true if the Shape argument is a child of the NestingShape object on which this method is called, false otherwise.
    public boolean contains(Shape child) {
        return nest.contains(child);
    }

}
