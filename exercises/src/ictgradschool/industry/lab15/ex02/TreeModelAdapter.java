package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Created by ylin183 on 31/05/2017.
 */
public class TreeModelAdapter implements TreeModel {

    private NestingShape _adaptee;

    public TreeModelAdapter(NestingShape root) {
        _adaptee = root;
    }

    @Override
    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object newObject = null;

        if (parent instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) parent;
            newObject = nestingShape.shapeAt(index);
        }

        return newObject;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape newShape = (Shape) parent;

        if (newShape instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) newShape;
            result = nestingShape.shapeCount();
        }

        return result;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;

        if (parent instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) parent;
            Shape shape = (Shape) child;
            indexOfChild = nestingShape.indexOf(shape);
        }

        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

}